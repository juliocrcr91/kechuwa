import React from 'react'
import logo from "../../Images/logo.png"
import kechuwa from "../../Images/kechuwa.png"
import { Container, Row, Image } from 'react-bootstrap'

export default function Home() {
  return (
    <div className="bghome">
      <Container className="text-center">
        <Row className="d-flex flex-column align-content-center justfy-content-center ">
          <div className="text-center">
            <Image src={logo} className="logosize" alt="" />
          </div>
          <div className="text-center">
            <Image src={kechuwa} className="wordsize" alt="" />
          </div>
          <div className="text-center">
            <h1 className="prox"> Proximamente</h1>
          </div>
        </Row>
      </Container>

    </div>
  )
}
